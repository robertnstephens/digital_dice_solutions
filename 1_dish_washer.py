#! /usr/bin/env python3

import random

# estimate prob that 5 dishes broken among 5 dishwashers, 4 by same one

prob_break = 1e-2
N = 10000
num_dw = 5
single_four_or_more = 0

for n in range(N):

    breakages = 0
    dw = [0 for _ in range(num_dw)]

    i = 0
    while 1:
        if random.uniform(0, 1) < prob_break:
            dw[i] += 1
            breakages += 1 
   
        if breakages == 5:
            if max(dw) >= 4:
                single_four_or_more += 1
            break

        i += 1
        i = i % num_dw

print(float(single_four_or_more)/N)
print( 21.0 / pow(5,4) )

