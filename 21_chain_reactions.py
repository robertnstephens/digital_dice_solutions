#! /usr/bin/env python3

import random

# prob. of 2 in second, 4 in second, 6 in third generation?

pn = [0.4825]
for i in range(1, 7):
    pn.append(pn[i-1] + 0.2126*pow(0.5893,i-1))
pn.append(1)


def one_generation(cur_m):
    next_m = 0
    for _ in range(cur_m):
        p = random.uniform(0, 1)
        for i in range(1, len(pn)):
            if pn[i-1] <= p < pn[i]:
                next_m += i
                break
    
    return next_m


generations = [0 for _ in range(10)]


prob_two_second, prob_four_second, prob_six_third = 0, 0, 0

N = 50000
for _ in range(N):
    cur_m = 1
    for i in range(0, len(generations)): 
        cur_m = one_generation(cur_m)
        generations[i] += cur_m

    
        if i == 1:
            if cur_m == 2:
                prob_two_second += 1
            if cur_m == 4:
                prob_four_second += 1
        if i == 2 and cur_m == 6:
            prob_six_third += 1


print("prob two in second", prob_two_second/N)
print("prob four in second", prob_four_second/N)
print("prob six in third", prob_six_third/N)

for g in generations:
    print(g/N)
