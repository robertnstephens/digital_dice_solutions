#! /usr/bin/env python3

# use Knuth's recurrences to calc M200(p) as p moves from 0 to 1
# I used dynamic programming (recusive caching) to greatly speed up execution

# Mn(p) = Mn,n(p)
# Mn,n(p) = Mn,n-1(p)
# Mm,n(p) = p*Mm-1,n(p) + q*Mm,n-1(q), where m > n > 0
# Mn,0 = n

def calc(p, m, n, dp):
    if n == 0:
        return m

    key = str(m) + "_" + str(n)
    if dp.get(key) == None:
        if m == n:
            res = calc(p, m, n-1, dp)
        elif m > n:
            res = p*calc(p, m-1, n, dp) + (1-p)*calc(p, m, n-1, dp)
        dp[key] = res
    return dp[key]

divs = 100
m = n = 200

for p_int in range(0, divs+1):
    p = float(p_int)/divs
    dp = {}
    res = calc(p, m, n, dp)
    print(p, res)

# test case where simple analytic solution exists
def run_simple():
    p = 0.9
    m, n = 3, 3
    res = calc(p, m, n, {})
    print(res)
    analytic_res = 3 - 2*p - p*p + p*p*p
    print(res, analytic_res) 
    if abs(res-analytic_res) < 1e-9:
        print("passed")
    else:
        print("failed")
#run_simple()
