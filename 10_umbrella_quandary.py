#! /usr/bin/env python3

# man carries umbrella if raining, doesn't if dry
# stores x at home, y at work
# how many trips before first getting wet?

import random, sys

x_start = y_start = 1
if len(sys.argv) > 1:
    x_start = y_start = int(sys.argv[1])

P = [float(i)/100 for i in range(1, 100)]
N = 10000

for p in P:

    sum_trips = 0

    for n in range(N):
        umbrellas = [x_start, y_start]
        t = 0

        while 1:
            if random.uniform(0,1) < p:
                if umbrellas[t%2] <= 0:
                    sum_trips += t
                    break
    
                umbrellas[t%2] -= 1
                umbrellas[(t+1)%2] += 1
            t+=1
    
    print(p, float(sum_trips)/N)

