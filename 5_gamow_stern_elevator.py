#! /usr/bin/env python3

import random, sys

num_elevators = 1
if len(sys.argv) > 1:
    num_elevators = int(sys.argv[1])

gamow_floor = 1.0/6
sum_going_down = 0

N = 10000
for n in range(N):

    distances = []
    arrival_dirs = []

    for i in range(num_elevators):
        dir_req = random.randint(0, 1)
        height = random.uniform(0, 1.0)

        if height < gamow_floor:
            if dir_req == 0:
                dist = gamow_floor + height
            else:
                dist = gamow_floor - height
            dir_arr = 1
        
        elif height > gamow_floor:
            if dir_req == 0:
                dist = height - gamow_floor
            else:
                dist = 2 - height - gamow_floor
            dir_arr = 0
  
        distances.append(dist)
        arrival_dirs.append(dir_arr)

    min_index = distances.index(min(distances))
   
    sum_going_down += 1.0 - arrival_dirs[min_index]

print(sum_going_down/N)
if num_elevators == 1:
    print(5.0/6, "(theoretical)")
elif num_elevators == 2:
    print(13.0/18, "(theoretical)")
else:
    pass


