#! /usr/bin/env python3

import random

num_cars = [i for i in range(4, 12)]
num_cars += [20, 30]

print(num_cars)

for nc in num_cars:

    N = 10000

    mutual_nn = 0
    for _ in range(N):
        cars = [random.uniform(0, 1) for _ in range(nc)] 
        cars.sort()
    
        nn_arr = [0 for _ in cars]
        nn_arr[0] = 1
        nn_arr[-1] = len(cars)-2 
    
        for i in range(1, len(cars)-1):
            if cars[i] - cars[i-1] < cars[i+1] - cars[i]:
                nn_arr[i] = i-1
            else:
                nn_arr[i] = i+1
    
        for j in range(len(cars)-1):
            if nn_arr[j] == j+1 and nn_arr[j+1] == j:
                mutual_nn += 2
    
    mutual_nn = float(mutual_nn) / N
    print(nc, mutual_nn, mutual_nn/nc)

