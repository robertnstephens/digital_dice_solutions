#! /usr/bin/env python3

import random

# b buses per hour, at unknown times
# arrive at bus stop at random time
# what is average waiting time vs b?
# we know for sure that one arrives on the hour


# generalize to one hour
B = [i+1 for i in range(5)]
av_wait_times_mins = [0 for _ in range(len(B))]
hour_seconds = 60*60

for b in B:

    sum_wait_time = 0

    N = 100000
    for n in range(N):

        arrival_time = random.uniform(0, hour_seconds-1)
        bus_times = random.sample(range(0, hour_seconds), b-1)
        bus_times.append(0)
        bus_times.append(hour_seconds)
        bus_times.sort()
    
        for i in range(1, len(bus_times)):
            if bus_times[i-1] < arrival_time <= bus_times[i]:
                sum_wait_time += bus_times[i] - arrival_time
                break

    av_wait_times_mins[b-1] = sum_wait_time/N

for i, wt in enumerate(av_wait_times_mins):
    b = i+1 
    print("buses per hour:", b, "av. wait time (mins):", str(wt/60)[:5], "guess of 60/(b+1):", 60/(b+1))

