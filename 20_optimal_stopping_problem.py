#! /usr/bin/env python3

# find optimal sample size for obtaining in top 1, in top 2 etc, of population

import random

def run_sim(pop_size, sample_size, top_to_check):
    population = [i for i in range(pop_size)]
    pop_order = population[-top_to_check:][::-1]
    random.shuffle(population)

    if sample_size == 0:
        best = -1
    else:
        best = max(population[:sample_size])

    for i in range(sample_size, len(population)):
        if population[i] > best:
            return [population[i] >= pop_val for pop_val in pop_order] 

    return [population[-1] >= pop_val for pop_val in pop_order] 

def run_sim_N(pop_size, sample_size, top_to_check):
    sum_top_to_check = [0 for _ in range(top_to_check)] 
    N = 50000
    for _ in range(N):
        res = run_sim(pop_size, sample_size, top_to_check)
        sum_top_to_check = [s+r for s, r in zip(sum_top_to_check, res)]
    return [s/N for s in sum_top_to_check]

pop_sizes = [11, 25, 50]

for pop_size in pop_sizes:
    print("population size:", pop_size) 

    opt_sample_sizes = [-1 for _ in range(pop_size)]

    top_to_check = 5
    opt_sample_sizes = [-1 for _ in range(top_to_check)]
    max_vals = [-1 for _ in range(top_to_check)]

    for sample_size in range(0, pop_size):
        res = run_sim_N(pop_size, sample_size, top_to_check)
       
        for i, r in enumerate(res):
            if r > max_vals[i]:
                max_vals[i] = r
                opt_sample_sizes[i] = sample_size
        print(sample_size, res)
    print("#"*50)
    print("optimal sample sizes for top 1-5:")
    print(opt_sample_sizes)
    print("#"*50)

