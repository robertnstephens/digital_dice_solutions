#! /usr/bin/env python3

import random, math

# estimate probability of selecting group leader with one election


def run_sim(electors, elector_subset, votes_needed, can_vote_for_self):
    votes = [0 for _ in range(elector_subset)]
    if can_vote_for_self: 
        for i in range(electors):
            votes[random.randint(0, elector_subset-1)] += 1
    else:        
        for i in range(electors-elector_subset):
            votes[random.randint(0, elector_subset-1)] += 1
        for i in range(elector_subset):
            idx = random.randint(0, elector_subset-1)
            while idx == i:
                idx = random.randint(0, elector_subset-1)
            votes[idx] += 1

    return max(votes) >= votes_needed

def run_sim_N(electors, elector_subset, votes_needed, can_vote_for_self):
    sum_prob = 0
    N = 100000
    for _ in range(N):
        sum_prob += run_sim(electors, elector_subset, votes_needed, can_vote_for_self)
    return sum_prob/N

elector_subsets = [2, 3, 4]

# imperial election
electors = 7
votes_needed = math.ceil(electors/2)

print("imperial election:")
for elector_subset in elector_subsets:
    for can_vote_for_self in [True, False]:
        prob = run_sim_N(electors, elector_subset, votes_needed, can_vote_for_self)
        print(str(prob), "subset n", elector_subset, "can vote for self:", can_vote_for_self)


# papal election
electors = 25
votes_needed = math.ceil(2*electors/3)

print("papal election:")
for elector_subset in elector_subsets:
    for can_vote_for_self in [True, False]:
        prob = run_sim_N(electors, elector_subset, votes_needed, can_vote_for_self)
        print(str(prob), "elector subset", elector_subset, "can vote for self:", can_vote_for_self)





