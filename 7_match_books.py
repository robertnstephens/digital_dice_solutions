#! /usr/bin/env python3

import random

N = 10000
sum_needed = 0

for n in range(N):

    match_books = [40, 40]
    matches_taken = 0

    while min(match_books) > 0:
        book = random.randint(0, 1)
        match_books[book] -= 1
        matches_taken += 1

    sum_needed += matches_taken

print( float(sum_needed) / N )




