#! /usr/bin/env python3

# 100 senators
# A < 50 are against a bill (for a majority 100-A > 50 are for it)
# M are missing on day of vote (same probability whether for or against the bill)
# what's the probability of the bill not passing?

import random, sys

S, A, M = 100, 49, 3

if len(sys.argv) == 3:
    A = int(sys.argv[1])
    M = int(sys.argv[2])

N = 100000

sum_fails = 0

for n in range(N):

    votes_for = S-A
    votes_against = A

    for m in range(M):
        p = float(votes_against)/(votes_for+votes_against)
        if random.uniform(0, 1) < p:
            votes_against -= 1
        else:
            votes_for -= 1

    if votes_against >= votes_for:
        sum_fails += 1

print( float(sum_fails)/N )
if A == 49 and M == 3:
    print(51.0/396, "(theoretical value)")


