#! /usr/bin/env python3

import random

# how many red lights will have to be waited through?

m_dest = n_dest = 1
av_red_lights = []

for m_start in range(1, 1002):

    n_start = m_start
    sum_red_lights = 0
    m, n = m_start, n_start

    N = 100
    for _ in range(N):
        red_lights = 0
        while 1:
            if m == m_dest or n == n_dest:
                if m == m_dest and n > n_dest:
                    red_lights = sum([random.uniform(0,1)<0.5 for _ in range(n-n_dest)]) 
                elif n == n_dest and m > m_dest:
                    red_lights = sum([random.uniform(0,1)<0.5 for _ in range(m-m_dest)]) 
                else:
                    red_lights = 0
                break             

            if random.uniform(0,1) < 0.5:
                m -= 1 
            else:
                n -= 1

        sum_red_lights += red_lights

    av_red_lights.append( sum_red_lights/N )
    
for rl in av_red_lights:
    print(rl)

