#! /usr/bin/env python3

import random
import matplotlib.pyplot as plt
import numpy as np

# exploring how accurate a sample percentage is at estimate population size
# population is unique numbers in range (1, N)
# N = ((n+1)/n)*E(max Xi) -1

sample_percent = [2, 5, 10, 20]
ests_per_percent = []

for percent in sample_percent:
   
    est_accs = []
    for _ in range(10000):
        N = random.randint(100, 1000)
        n = int(percent*N/100)

        population = np.random.permutation(range(1, N+1))
        samples = []

        cur_N, cur_n = N, n
        for i in range(len(population)):
            p = cur_n/cur_N
            if random.uniform(0,1) < p:
                samples.append( population[i] )
                cur_n -= 1
            cur_N -= 1

        N_est = ((n+1)/n)*max(samples)-1
        est_accs.append(100.0*(N_est-N)/N)
   
    ests_per_percent.append( est_accs )
 
bins = np.linspace(-25, 25, 100)

for i, percent in enumerate(sample_percent):

    # the histogram of the data
    #m, bins, patches = plt.hist(ests_per_percent[i], 100, normed=1, facecolor='g', alpha=0.75)
    m, bins, patches = plt.hist(ests_per_percent[i], bins, normed=1, alpha=0.75, label=str(percent)+" percent")

plt.legend(loc='upper right')

plt.xlabel('percent accuracy')
plt.ylabel('count')
plt.title('histogram of population estimate error for 2, 5, 10, 20% sample size')
#plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
#plt.axis([40, 160, 0, 0.03])
plt.grid(True)
#plt.show()
    
plt.savefig("12_graph_pop_estimate_vs_percent_sample")

