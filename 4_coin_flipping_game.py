#! /usr/bin/env python3

import random, copy

p = 0.5
players = 5
#start_coins = [i+1 for i in range(players)]
#start_coins = [4, 7, 9]
start_coins = [random.randint(1, 10) for _ in range(players)]

sum_throws = 0
N = 10000
for n in range(N):
   
    coins = copy.deepcopy(start_coins)
    while min(coins) > 0:
        flips = [random.uniform(0, 1) > p for _ in range(players)] 
 
        sum_throws += 1

        if sum(flips) == players or sum(flips) == 0:
            continue

        if sum(flips) == players-1: 
            coins = [c-1 for c in coins]
            coins[flips.index(0)] += 3 
        elif sum(flips) == 1: 
            coins = [c-1 for c in coins]
            coins[flips.index(1)] += 3 

print(start_coins)
print(float(sum_throws)/N)

