#! /usr/bin/env python3

import random

# two games involving coin flipping
# game A: heads win $1, prob. 0.5 - epsilon, tails lose $1
# game B: heads win $1 - if money M%3 == 0 prob. 0.1 - epsilon, else prob. 0.75 - epsilon
# show for game B, on average will lose money, along with for game A trivially 
# yet switching randomly between A and B, will not!

epsilon = 0.005
M = 0
num_flips = 100
num_sequences = 1000
ab_ensemble_sum = [0 for _ in range(num_flips)]
b_ensemble_sum = [0 for _ in range(num_flips)]

def run_game_a(money):
    if random.uniform(0,1) < 0.5-epsilon:
        money += 1
    else:
        money -= 1
    return money

def run_game_b(money):
    if money%3 == 0:
        heads_prob = 0.1-epsilon
    else:
        heads_prob = 0.75-epsilon

    if random.uniform(0,1) < heads_prob:
        money += 1 
    else:
        money -= 1
    return money

for _ in range(num_sequences):
    ab_money = M
    b_money = M

    for k in range(num_flips):
        if random.uniform(0,1) < 0.5:
            ab_money = run_game_a(ab_money)
        else:
            ab_money = run_game_b(ab_money)

        ab_ensemble_sum[k] += ab_money

        b_money = run_game_b(b_money)
        b_ensemble_sum[k] += b_money


b_ensemble_average = [es/num_sequences for es in b_ensemble_sum]
ab_ensemble_average = [es/num_sequences for es in ab_ensemble_sum]

for ab, b in zip(ab_ensemble_average, b_ensemble_average):
    print(ab, b)

