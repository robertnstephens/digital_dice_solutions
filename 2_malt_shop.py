#! /usr/bin/env python3

import random

l_wait, b_wait = 5, 7

T = 30

N = 10000
meetings = 0

for n in range(N):
    l_arrive = random.uniform(0, T)
    l_exit = l_arrive + l_wait
    l_exit = min(l_exit, T)
    
    b_arrive = random.uniform(0, T)
    b_exit = b_arrive + b_wait
    b_exit = min(b_exit, T)

    if l_arrive <= b_arrive <= l_exit or b_arrive <= l_arrive <= b_exit:
        meetings += 1

print( float(meetings)/N )


