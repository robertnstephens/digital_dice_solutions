#! /usr/bin/env python3

import random

k = 9
floors = 11
steves_floor = floors-1-2 # he has two floors above

N = 100000

sum_stops = 0

for n in range(N):

    stops = [random.randint(0, floors-1) for _ in range(k)]

    stops_below = [s for s in stops if s < steves_floor] 

    # always at least one - Steve's stop
    sum_stops += 1 + len(set(stops_below))    
    
print( float(sum_stops)/N )






