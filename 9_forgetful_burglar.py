#! /usr/bin/env python3

import random

# infinite line of towns
# can move 1 or 2 in either direction with equal probability
# average steps before repeat visit?
# prob of repeat for k in range 1 <= k <= 7?

N = 100000
k_list = [0 for _ in range(7)]
sum_steps = 0

for n in range(N):
    cur_town = 0
    visited = {cur_town:True}
    k = 0

    while 1:
        dist = random.randint(1, 2)
        if random.randint(0,1) == 0:
            cur_town -= dist
        else:
            cur_town += dist
        if visited.get(cur_town) != None:
            sum_steps += k
            if k < len(k_list):
                k_list[k] += 1
            break
        else: 
            visited[cur_town] = True
        k +=1

print("average steps before repeat:", float(sum_steps)/N)
k_probs = [float(step_sum)/N for step_sum in k_list]
print(k_probs)
