#! /usr/bin/env python3

import random, copy

# majority >= 3 required to make decision
# compare all being independent with most unreliable judge always voting with first
# probability of error in judgement actually increases!

judges_correct_prob = [0.95, 0.95, 0.9, 0.9, 0.8]
sum_correct_all, sum_correct_last_with_first = 0, 0

N = 100000
for n in range(N):
    decision_all, decision_last_with_first = [], []
    
    for judge_correct in judges_correct_prob:
        if random.uniform(0,1) < judge_correct:
            decision_all.append(1)
        else: 
            decision_all.append(0)

    # for these, more unreliable judge always votes with first
    decision_last_with_first = copy.deepcopy(decision_all)
    decision_last_with_first[-1] = decision_last_with_first[0]

    if sum(decision_all) >= 3:
        sum_correct_all += 1

    if sum(decision_last_with_first) >= 3:
        sum_correct_last_with_first += 1

print(1 - sum_correct_all/N, "(error, with all being independent)")
print(1 - sum_correct_last_with_first/N, "(error, with most unreliable judge always voting with first)")



